from django.conf.urls import url, include


app_name = 'api_v1'
urlpatterns = [
    url(r'', include('hotel_widget.hotel.urls')),
]
