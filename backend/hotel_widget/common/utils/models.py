import csv


def fill_city_model_based_on_csv(csv_file_path, city_model=None):
    if not city_model:
        from hotel_widget.hotel.models import City
    else:
        City = city_model

    with open(csv_file_path) as csv_file:
        reader = csv.DictReader(csv_file)
        cities_name = {row['city_name_ru'] for row in reader}

    City.objects.bulk_create([
        City(name=city_name) for city_name in cities_name
    ])


def fill_hotel_model_based_on_csv(csv_file_path, hotel_model=None, city_model=None):
    if not hotel_model:
        from hotel_widget.hotel.models import Hotel
    else:
        Hotel = hotel_model

    if not city_model:
        from hotel_widget.hotel.models import City
    else:
        City = city_model

    with open(csv_file_path) as csv_file:
        reader = csv.DictReader(csv_file)

        hotels = []
        for row in reader:
            hotels.append(
                Hotel(
                    city=City.objects.get(name=row['city_name_ru']),
                    name=row['hotel_name'],
                    image_url=row['image_url'],
                    price=row['price'],
                )
            )
    Hotel.objects.bulk_create(hotels)
