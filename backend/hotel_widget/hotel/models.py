from django.db import models


class City(models.Model):
    name = models.CharField('Название', max_length=200, db_index=True)

    class Meta:
        verbose_name = 'Город'
        verbose_name_plural = 'Города'

    def __str__(self):
        return self.name


class Hotel(models.Model):
    city = models.ForeignKey('City', on_delete=models.CASCADE, related_name='hotels')
    name = models.CharField('Название отеля', max_length=200)
    price = models.DecimalField('Цена', max_digits=8, decimal_places=2, db_index=True)
    image_url = models.URLField('Картинка')

    class Meta:
        verbose_name = 'Отель'
        verbose_name_plural = 'Отели'
        ordering = ['price']

    def __str__(self):
        return self.name
