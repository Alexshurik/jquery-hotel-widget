from django.urls import reverse
from model_mommy import mommy
from rest_framework import status
from rest_framework.test import APITestCase

from hotel_widget.hotel.models import City

from hotel_widget.hotel.models import Hotel


class HotelsTestCase(APITestCase):
    def test_get_privacy_policy(self):
        city1 = mommy.make(City, name='our city')
        city2 = mommy.make(City, name='some other city')

        # Эти отели должны найтись по запросу "our city"
        hotel1 = mommy.make(Hotel, city=city1)
        hotel2 = mommy.make(Hotel, city=city1)
        hotel3 = mommy.make(Hotel, city=city1)

        # А эти нет
        mommy.make(Hotel, city=city2)
        mommy.make(Hotel, city=city2)
        mommy.make(Hotel, city=city2)

        url = reverse('api-v1:hotel-list')
        response = self.client.get(url, {'city__name': 'our city'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        retrieved_hotel_ids = [
            hotel['id'] for hotel in response.data['results']
        ]
        self.assertCountEqual(retrieved_hotel_ids, [hotel1.pk, hotel2.pk, hotel3.pk])
