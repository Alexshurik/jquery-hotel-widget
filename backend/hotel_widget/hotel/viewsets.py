from rest_framework import viewsets, mixins

from hotel_widget.common.pagination import StandardPagination
from hotel_widget.hotel.serializers import HotelSerializer

from hotel_widget.hotel.models import Hotel


class HotelViewSet(viewsets.GenericViewSet, mixins.ListModelMixin):
    serializer_class = HotelSerializer
    pagination_class = StandardPagination
    queryset = Hotel.objects.select_related('city')
    filter_fields = ('city__name',)
