from django.contrib import admin

from hotel_widget.hotel.models import City
from hotel_widget.hotel.models import Hotel


@admin.register(Hotel)
class HotelAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'name',
        'city',
        'price',
    )
    search_fields = (
        'name',
        'city',
    )


@admin.register(City)
class CityAdmin(admin.ModelAdmin):
    list_display = (
        'name',
    )
    search_fields = (
        'name',
    )
