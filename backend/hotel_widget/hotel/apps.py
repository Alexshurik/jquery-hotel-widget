from django.apps import AppConfig


class HotelConfig(AppConfig):
    name = 'hotel_widget.hotel'
    verbose_name = 'Отель'
