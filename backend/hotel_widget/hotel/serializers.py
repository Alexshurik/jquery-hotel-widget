from rest_framework import serializers

from hotel_widget.hotel.models import Hotel


class HotelSerializer(serializers.ModelSerializer):
    city = serializers.SlugRelatedField(slug_field='name', read_only=True)

    class Meta:
        model = Hotel
        fields = (
            'id',
            'city',
            'name',
            'price',
            'image_url',
        )
