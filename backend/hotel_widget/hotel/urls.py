from django.conf.urls import url
from django.urls import include
from rest_framework import routers

from hotel_widget.hotel.viewsets import HotelViewSet


router = routers.SimpleRouter()
router.register('hotels', HotelViewSet, base_name='hotel')

urlpatterns = [
    url(r'^', include(router.urls)),
]
