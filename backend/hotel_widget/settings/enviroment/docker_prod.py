try:
    from hotel_widget.settings.enviroment import sensitive_data
except Exception as e:
    raise ImportError('Отсутствует файл "sensitive_data". См11. пример в settings/enviroment/sensitive_data.example')

ALLOWED_HOSTS = [
    '127.0.0.1',
    '0.0.0.0',
    '138.197.187.203',
]

DEBUG = False

SECRET_KEY = sensitive_data.PROD_SECRET_KEY

DATABASES = sensitive_data.PROD_DATABASES

CORS_ORIGIN_ALLOW_ALL = True
