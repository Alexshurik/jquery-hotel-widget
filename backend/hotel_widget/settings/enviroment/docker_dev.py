DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'hotel_widget',
        'USER': 'hotel_widget',
        'PASSWORD': 'hotel_widget',
        'HOST': 'postgres',
        'PORT': '5432',
    }
}

ALLOWED_HOSTS = [
    '*'
]

CORS_ORIGIN_ALLOW_ALL = True
