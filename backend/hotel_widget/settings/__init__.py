from os import environ

# Импорт общих настроек
from .common import *


# Импорт настроек, специфичных для окружения
environ.setdefault('DJANGO_ENVIROMENT_SETTINGS', 'local_dev')
ENV = environ['DJANGO_ENVIROMENT_SETTINGS']

if ENV == 'docker_dev':
    from .enviroment.docker_dev import *
elif ENV == 'docker_prod':
    from .enviroment.docker_prod import *
else:
    from .enviroment.local_dev import *

# Импорт локальных настроек (см. local.example.py)
try:
    from hotel_widget.settings.enviroment.local import *
except:
    pass
