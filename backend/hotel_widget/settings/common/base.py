import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

DOCS_DIR = os.path.join(os.path.dirname(BASE_DIR), 'docs')

HOTELS_CSV_DATA_FILE = os.path.join(DOCS_DIR, 'hotel_list.csv')

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.11/howto/deployment/checklist/

DEBUG = False

ROOT_URLCONF = 'hotel_widget.urls'

WSGI_APPLICATION = 'hotel_widget.wsgi.application'

ADMINS = [
    ('hotel_widget', 'hotel_widget@mail.ru'),
]

MEDIA_URL = '/media/'
MEDIA_ROOT = '/var/hotel_widget/media/'

STATIC_URL = '/static/'
STATIC_ROOT = '/var/hotel_widget/static/'

SECRET_KEY = 'dont use me in production))'
