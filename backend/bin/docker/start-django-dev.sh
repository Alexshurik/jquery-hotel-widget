#!/bin/bash

set -o errexit
set -o pipefail
set -o nounset
set -o xtrace

/home/app/manage.py migrate
/home/app/manage.py runserver 0.0.0.0:8000