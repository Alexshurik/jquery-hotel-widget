# Common
Django==2.0.2
psycopg2-binary==2.7.4
djangorestframework==3.7.7
django-filter==1.1.0
django-cors-headers==2.2.0

#Production
gunicorn==19.6.0

# Development
django-debug-toolbar
ipython
readline
django-extensions
Werkzeug
watchdog
coverage
model_mommy
