export const WIDGET_CONTAINER_WRAPPER_SELECTOR = '#hotel-widget-container-wrapper';
export const CITY_DROPDOWN_WRAPPER = '.js-city-dropdown-wrapper';
export const CITY_DROPDOWN_LIST = '.js-city-dropdown-list';
export const HOTEL_LIST = '.js-hotel-list';