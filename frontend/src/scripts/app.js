import hotelApi from "./api/hotels/index";
import Hotel from "./components/Hotel";
import {setUpjQueryAjax} from "./api/utils";
import * as selectors from "./constants/selectors"
import Widget from "./components/Widget";
import config from './api/config';


let {FRONT_PROTOCOL, FRONT_HOST, FRONT_PORT } = config;

let $, jQuery;


export default class App {
    constructor(jQueryObject) {
        $ = jQuery = jQueryObject;
        setUpjQueryAjax($);

        this.appendWidget();
    }

    appendWidget() {
        this.$widgetWrapper = $(selectors.WIDGET_CONTAINER_WRAPPER_SELECTOR);
        if (this.$widgetWrapper.length === 0) {
            throw Error(
                `Невозможно подключить виджет: на страницу отстутсвует элемент с
                 ${selectors.WIDGET_CONTAINER_WRAPPER_SELECTOR}`
            )
        }

        let cssLinkElement = $('<link>', {
           rel: 'stylesheet',
           type: 'text/css',
           href: `${FRONT_PROTOCOL}://${FRONT_HOST}:${FRONT_PORT}/style.css`
         });
        cssLinkElement.appendTo('head');

        cssLinkElement.on('load', () => {
            this.$widgetWrapper.append($.parseHTML(Widget()));
            this.initializeApp();
        });
    }

    initializeApp() {
        this.queryElements();
        this.initializeCitiesDropdown();
        this.initializeHotelList();
    }

    queryElements() {
        this.$cityDropdown = this.$widgetWrapper.find(selectors.CITY_DROPDOWN_WRAPPER);
        this.$citySearchInput = this.$cityDropdown.find('input');

        this.$cityDropdownList = this.$cityDropdown.find(selectors.CITY_DROPDOWN_LIST);
        this.$cityDropdownListItems = this.$cityDropdown.find('li');

        this.$hotelList = $(selectors.HOTEL_LIST);
    }

    initializeCitiesDropdown() {
        this.$citySearchInput.on('input', (event) => {
            this.$cityDropdownListItems.each((_, listItem) => {
                let $listItem = $(listItem);
                let itemText = $listItem.text().toLowerCase();

                if (itemText.startsWith(this.$citySearchInput.val().toLowerCase())) {
                    $listItem.show();
                } else {
                    $listItem.hide();
                }
            });
        });

        this.$citySearchInput.click(() => {
            this.$cityDropdown.toggleClass('active');
            this.$cityDropdownList.slideToggle(300);
        });

        this.$cityDropdown.focusout(() => {
            this.$cityDropdown.removeClass('active');
            this.$cityDropdownList.slideUp(300);
        });
    }

    _loadHotels(cityName, pageNumber=1, appendHotels=false) {
        hotelApi.get($, cityName, pageNumber).done((data) => {
            let hotels = data['results'].map(Hotel);

            if (!appendHotels) {
                this.$hotelList.empty();
            }
            this.$hotelList.unbind('scroll');
            this.$hotelList.append(hotels);

            if (data['next']) {
                this.$hotelList.on('scroll', () => {
                    let scrollPercentage = (this.$hotelList[0].scrollTop + this.$hotelList[0].clientHeight)
                        / this.$hotelList[0].scrollHeight;

                    if (scrollPercentage > 0.9) {
                        this.$hotelList.unbind('scroll');
                        this._loadHotels(cityName, pageNumber + 1, true)
                    }
                });
            }
        })
    };

    initializeHotelList() {
        this.$cityDropdownListItems.click((event) => {
            let cityName = $(event.target).text();
            this.$citySearchInput.val(cityName);
            this._loadHotels(cityName)
        });
    }
}