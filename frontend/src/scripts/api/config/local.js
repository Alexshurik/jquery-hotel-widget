const API_PROTOCOL = 'http';
const API_HOST = '127.0.0.1';
const API_PORT = '8000';
const API_BASE_PATH = '/api/v1/';

const FRONT_PROTOCOL = 'http';
const FRONT_HOST = '127.0.0.1';
const FRONT_PORT = '3000';

module.exports = {
    API_PROTOCOL,
    API_HOST,
    API_PORT,
    API_BASE_PATH,
    FRONT_PROTOCOL,
    FRONT_HOST,
    FRONT_PORT,
};
