const API_PROTOCOL = 'http';
const API_HOST = '138.197.187.203';
const API_PORT = '80';
const API_BASE_PATH = '/api/v1/';

// ToDo: вынести
// Не самое лучшая папка (api/config) для хранения настроек фронта
const FRONT_PROTOCOL = 'http';
const FRONT_HOST = '138.197.187.203';
const FRONT_PORT = '80';

module.exports = {
    API_PROTOCOL,
    API_HOST,
    API_PORT,
    API_BASE_PATH,
    FRONT_PROTOCOL,
    FRONT_HOST,
    FRONT_PORT,
};
