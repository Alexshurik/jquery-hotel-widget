let config_module;

// ToDo: переделать
// В реальном приложение можно проставить константу "API_ENV" с помощью webpack или grunt/gulp
// на основе ENV-переменной (которая устанавливается с помощью конфиг. файла docker-compose)
// На основе константы "API_ENV" подгружать нужны конфиг

// Здесь же подругажаю либо local, либоа prod в зависимости от hostnamt
switch (process.env.API_CONFIG_ENV) {
    case 'docker_dev':
        config_module = require('./docker_dev');
        break;
    case 'docker_prod':
        config_module = require('./docker_prod');
        break;
    default:
        config_module = require('./local');
}

export default config_module;
