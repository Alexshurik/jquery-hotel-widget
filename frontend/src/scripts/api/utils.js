import config from './config';

let {API_PROTOCOL, API_HOST, API_PORT, API_BASE_PATH} = config;

export const setUpjQueryAjax = ($) => {
    $.ajaxSetup({
      url: `${API_PROTOCOL}://${API_HOST}:${API_PORT}${API_BASE_PATH}`,
    });
};