export default {
    get: ($, cityName, pageNumber=1) => {
        return $.get(
            `${$.ajaxSettings.url}hotels/`,
            {'city__name': cityName, 'page': pageNumber}
        )
    }

}
