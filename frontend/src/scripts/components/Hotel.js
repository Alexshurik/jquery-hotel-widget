export default ({name, image_url, city, price}) => {
    return `
        <li class="hotel-item js-hotel-list-item">
          <img src="${image_url}" alt="${name}">
          <div class="hotel-description">
            <span class="hotel-description-name">${name}</span>
            <span class="hotel-description-city">${city}</span>
            <span class="hotel-description-price">${price}</span>
          </div>
        </li>
    `
}