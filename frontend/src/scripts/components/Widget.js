export default () => {
    return `
    <div id="hotel-widget-container">
        <div>
        <span class="city-choose-text">Выберите город</span>
        <div class="city-dropdown-wrapper js-city-dropdown-wrapper">
            <input class="city-search-input" type="text" name="city" placeholder="Выберите город">
            <ul class="city-dropdown-list js-city-dropdown-list">
            <li>Ростов-на-Дону</li>
            <li>Мюнхен</li>
            <li>Казань</li>
            <li>Самара</li>
            <li>Шарджа</li>
            <li>Лоо</li>
            <li>Ялта</li>
            <li>Нячанг</li>
            <li>Красная Поляна</li>
            <li>Милан</li>
            <li>Витязево</li>
            <li>Гонконг</li>
            <li>Евпатория</li>
            <li>Хельсинки</li>
            <li>Будапешт</li>
            <li>Лазаревское</li>
            <li>Псков</li>
            <li>Новосибирск</li>
            <li>Венеция</li>
            <li>Нью-Йорк</li>
            <li>Одесса</li>
            <li>Санкт-Петербург</li>
            <li>Паттайя</li>
            <li>Берлин</li>
            <li>Кисловодск</li>
            <li>Днепропетровск</li>
            <li>Хургада</li>
            <li>Хабаровск</li>
            <li>Шарм-эль-Шейх</li>
            <li>Сингапур</li>
            <li>Большой Геленджик</li>
            <li>Стамбул</li>
            <li>Варшава</li>
            <li>Лас-Вегас</li>
            <li>Москва</li>
            <li>Суздаль</li>
            <li>Петрозаводск</li>
            <li>Самуи</li>
            <li>Челябинск</li>
            <li>Амстердам</li>
            <li>Львов</li>
            <li>Нижний Новгород</li>
            <li>Минск</li>
            <li>Эйлат</li>
            <li>Пятигорск</li>
            <li>Анапа</li>
            <li>Киев</li>
            <li>Геленджик</li>
            <li>Париж</li>
            <li>Тюмень</li>
            <li>Ярославль</li>
            <li>Лондон</li>
            <li>Пермь</li>
            <li>Тель-Авив</li>
            <li>Судак</li>
            <li>Владимир</li>
            <li>Рим</li>
            <li>Шаркс Бей</li>
            <li>Джемете</li>
            <li>Вена</li>
            <li>Кабардинка</li>
            <li>Красноярск</li>
            <li>Великий Новгород</li>
            <li>Эсто-Садок</li>
            <li>Сочи</li>
            <li>Кемер</li>
            <li>Екатеринбург</li>
            <li>Бангкок</li>
            <li>Великий Устюг</li>
            <li>Иркутск</li>
            <li>Коктебель</li>
            <li>Таллин</li>
            <li>Пхукет</li>
            <li>Мадрид</li>
            <li>Родос</li>
            <li>Барселона</li>
            <li>Харьков</li>
            <li>Гоа</li>
            <li>Сеул</li>
            <li>Калининград</li>
            <li>Краснодар</li>
            <li>Канкун</li>
            <li>Тбилиси</li>
            <li>Адлер</li>
            <li>Рига</li>
            <li>Дубай</li>
            <li>Тула</li>
            <li>Омск</li>
            <li>Домбай</li>
            <li>Севастополь</li>
            <li>Уфа</li>
            <li>Симферополь</li>
            <li>Вильнюс</li>
            <li>Светлогорск</li>
            <li>Феодосия</li>
            <li>Воронеж</li>
            <li>Волгоград</li>
            <li>Алупка</li>
            <li>Алушта</li>
            <li>Прага</li>
            <li>Костром</li>
          </ul>
        </div>
        </div>
        
        <ul class="hotel-list js-hotel-list">
        </ul>
    </div>
    `
}