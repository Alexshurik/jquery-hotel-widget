import {JQUERY_VERSION_TO_LOAD} from "./constants/common";

const loadjQuery = (verison, onLoadHandler) => {
    let scriptTag = document.createElement('script');
    scriptTag.setAttribute('type', 'text/javascript');
    scriptTag.setAttribute('src', `http://ajax.googleapis.com/ajax/libs/jquery/${verison}/jquery.min.js`);

    if (scriptTag.readyState) {
        // For old versions of IE
        scriptTag.onreadystatechange = function () {
            if (this.readyState === 'complete' || this.readyState === 'loaded') {
                onLoadHandler();
            }
        };
    } else { // Other browsers
        scriptTag.onload = onLoadHandler;
    }

    (document.getElementsByTagName('head')[0] || document.documentElement).appendChild(scriptTag);
};


export const getjQuery = (onRetrieveHandler) => {
    // ToDo: проверить, возможно, в некоторых версиях не работает
    if (window.jQuery === undefined) {
        loadjQuery(JQUERY_VERSION_TO_LOAD, () => {
            let jQuery = window.jQuery.noConflict(true);
            onRetrieveHandler(jQuery);
        })
    } else {
        onRetrieveHandler(window.jQuery);
    }
};
