(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
// shim for using process in browser
var process = module.exports = {};

// cached from whatever global is present so that test runners that stub it
// don't break things.  But we need to wrap it in a try catch in case it is
// wrapped in strict mode code which doesn't define any globals.  It's inside a
// function because try/catches deoptimize in certain engines.

var cachedSetTimeout;
var cachedClearTimeout;

function defaultSetTimout() {
    throw new Error('setTimeout has not been defined');
}
function defaultClearTimeout () {
    throw new Error('clearTimeout has not been defined');
}
(function () {
    try {
        if (typeof setTimeout === 'function') {
            cachedSetTimeout = setTimeout;
        } else {
            cachedSetTimeout = defaultSetTimout;
        }
    } catch (e) {
        cachedSetTimeout = defaultSetTimout;
    }
    try {
        if (typeof clearTimeout === 'function') {
            cachedClearTimeout = clearTimeout;
        } else {
            cachedClearTimeout = defaultClearTimeout;
        }
    } catch (e) {
        cachedClearTimeout = defaultClearTimeout;
    }
} ())
function runTimeout(fun) {
    if (cachedSetTimeout === setTimeout) {
        //normal enviroments in sane situations
        return setTimeout(fun, 0);
    }
    // if setTimeout wasn't available but was latter defined
    if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
        cachedSetTimeout = setTimeout;
        return setTimeout(fun, 0);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedSetTimeout(fun, 0);
    } catch(e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
            return cachedSetTimeout.call(null, fun, 0);
        } catch(e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
            return cachedSetTimeout.call(this, fun, 0);
        }
    }


}
function runClearTimeout(marker) {
    if (cachedClearTimeout === clearTimeout) {
        //normal enviroments in sane situations
        return clearTimeout(marker);
    }
    // if clearTimeout wasn't available but was latter defined
    if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
        cachedClearTimeout = clearTimeout;
        return clearTimeout(marker);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedClearTimeout(marker);
    } catch (e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
            return cachedClearTimeout.call(null, marker);
        } catch (e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
            // Some versions of I.E. have different rules for clearTimeout vs setTimeout
            return cachedClearTimeout.call(this, marker);
        }
    }



}
var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
    if (!draining || !currentQueue) {
        return;
    }
    draining = false;
    if (currentQueue.length) {
        queue = currentQueue.concat(queue);
    } else {
        queueIndex = -1;
    }
    if (queue.length) {
        drainQueue();
    }
}

function drainQueue() {
    if (draining) {
        return;
    }
    var timeout = runTimeout(cleanUpNextTick);
    draining = true;

    var len = queue.length;
    while(len) {
        currentQueue = queue;
        queue = [];
        while (++queueIndex < len) {
            if (currentQueue) {
                currentQueue[queueIndex].run();
            }
        }
        queueIndex = -1;
        len = queue.length;
    }
    currentQueue = null;
    draining = false;
    runClearTimeout(timeout);
}

process.nextTick = function (fun) {
    var args = new Array(arguments.length - 1);
    if (arguments.length > 1) {
        for (var i = 1; i < arguments.length; i++) {
            args[i - 1] = arguments[i];
        }
    }
    queue.push(new Item(fun, args));
    if (queue.length === 1 && !draining) {
        runTimeout(drainQueue);
    }
};

// v8 likes predictible objects
function Item(fun, array) {
    this.fun = fun;
    this.array = array;
}
Item.prototype.run = function () {
    this.fun.apply(null, this.array);
};
process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues
process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;
process.prependListener = noop;
process.prependOnceListener = noop;

process.listeners = function (name) { return [] }

process.binding = function (name) {
    throw new Error('process.binding is not supported');
};

process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};
process.umask = function() { return 0; };

},{}],2:[function(require,module,exports){
'use strict';

var API_PROTOCOL = 'http';
var API_HOST = '127.0.0.1';
var API_PORT = '8000';
var API_BASE_PATH = '/api/v1/';

var FRONT_PROTOCOL = 'http';
var FRONT_HOST = '127.0.0.1';
var FRONT_PORT = '3000';

module.exports = {
    API_PROTOCOL: API_PROTOCOL,
    API_HOST: API_HOST,
    API_PORT: API_PORT,
    API_BASE_PATH: API_BASE_PATH,
    FRONT_PROTOCOL: FRONT_PROTOCOL,
    FRONT_HOST: FRONT_HOST,
    FRONT_PORT: FRONT_PORT
};

},{}],3:[function(require,module,exports){
'use strict';

var API_PROTOCOL = 'http';
var API_HOST = '138.197.187.203';
var API_PORT = '80';
var API_BASE_PATH = '/api/v1/';

// ToDo: вынести
// Не самое лучшая папка (api/config) для хранения настроек фронта
var FRONT_PROTOCOL = 'http';
var FRONT_HOST = '138.197.187.203';
var FRONT_PORT = '80';

module.exports = {
    API_PROTOCOL: API_PROTOCOL,
    API_HOST: API_HOST,
    API_PORT: API_PORT,
    API_BASE_PATH: API_BASE_PATH,
    FRONT_PROTOCOL: FRONT_PROTOCOL,
    FRONT_HOST: FRONT_HOST,
    FRONT_PORT: FRONT_PORT
};

},{}],4:[function(require,module,exports){
(function (process){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
var config_module = void 0;

// ToDo: переделать
// В реальном приложение можно проставить константу "API_ENV" с помощью webpack или grunt/gulp
// на основе ENV-переменной (которая устанавливается с помощью конфиг. файла docker-compose)
// На основе константы "API_ENV" подгружать нужны конфиг

// Здесь же подругажаю либо local, либоа prod в зависимости от hostnamt
switch (process.env.API_CONFIG_ENV) {
    case 'docker_dev':
        config_module = require('./docker_dev');
        break;
    case 'docker_prod':
        config_module = require('./docker_prod');
        break;
    default:
        config_module = require('./local');
}

exports.default = config_module;

}).call(this,require('_process'))
},{"./docker_dev":2,"./docker_prod":3,"./local":5,"_process":1}],5:[function(require,module,exports){
'use strict';

var API_PROTOCOL = 'http';
var API_HOST = '127.0.0.1';
var API_PORT = '8000';
var API_BASE_PATH = '/api/v1/';

var FRONT_PROTOCOL = 'http';
var FRONT_HOST = '127.0.0.1';
var FRONT_PORT = '3000';

module.exports = {
    API_PROTOCOL: API_PROTOCOL,
    API_HOST: API_HOST,
    API_PORT: API_PORT,
    API_BASE_PATH: API_BASE_PATH,
    FRONT_PROTOCOL: FRONT_PROTOCOL,
    FRONT_HOST: FRONT_HOST,
    FRONT_PORT: FRONT_PORT
};

},{}],6:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    get: function get($, cityName) {
        var pageNumber = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 1;

        return $.get($.ajaxSettings.url + 'hotels/', { 'city__name': cityName, 'page': pageNumber });
    }

};

},{}],7:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.setUpjQueryAjax = undefined;

var _config = require('./config');

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var API_PROTOCOL = _config2.default.API_PROTOCOL,
    API_HOST = _config2.default.API_HOST,
    API_PORT = _config2.default.API_PORT,
    API_BASE_PATH = _config2.default.API_BASE_PATH;
var setUpjQueryAjax = exports.setUpjQueryAjax = function setUpjQueryAjax($) {
  $.ajaxSetup({
    url: API_PROTOCOL + '://' + API_HOST + ':' + API_PORT + API_BASE_PATH
  });
};

},{"./config":4}],8:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _index = require("./api/hotels/index");

var _index2 = _interopRequireDefault(_index);

var _Hotel = require("./components/Hotel");

var _Hotel2 = _interopRequireDefault(_Hotel);

var _utils = require("./api/utils");

var _selectors = require("./constants/selectors");

var selectors = _interopRequireWildcard(_selectors);

var _Widget = require("./components/Widget");

var _Widget2 = _interopRequireDefault(_Widget);

var _config = require("./api/config");

var _config2 = _interopRequireDefault(_config);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var FRONT_PROTOCOL = _config2.default.FRONT_PROTOCOL,
    FRONT_HOST = _config2.default.FRONT_HOST,
    FRONT_PORT = _config2.default.FRONT_PORT;


var $ = void 0,
    jQuery = void 0;

var App = function () {
    function App(jQueryObject) {
        _classCallCheck(this, App);

        $ = jQuery = jQueryObject;
        (0, _utils.setUpjQueryAjax)($);

        this.appendWidget();
    }

    _createClass(App, [{
        key: "appendWidget",
        value: function appendWidget() {
            var _this = this;

            this.$widgetWrapper = $(selectors.WIDGET_CONTAINER_WRAPPER_SELECTOR);
            if (this.$widgetWrapper.length === 0) {
                throw Error("\u041D\u0435\u0432\u043E\u0437\u043C\u043E\u0436\u043D\u043E \u043F\u043E\u0434\u043A\u043B\u044E\u0447\u0438\u0442\u044C \u0432\u0438\u0434\u0436\u0435\u0442: \u043D\u0430 \u0441\u0442\u0440\u0430\u043D\u0438\u0446\u0443 \u043E\u0442\u0441\u0442\u0443\u0442\u0441\u0432\u0443\u0435\u0442 \u044D\u043B\u0435\u043C\u0435\u043D\u0442 \u0441\n                 " + selectors.WIDGET_CONTAINER_WRAPPER_SELECTOR);
            }

            var cssLinkElement = $('<link>', {
                rel: 'stylesheet',
                type: 'text/css',
                href: FRONT_PROTOCOL + "://" + FRONT_HOST + ":" + FRONT_PORT + "/style.css"
            });
            cssLinkElement.appendTo('head');

            cssLinkElement.on('load', function () {
                _this.$widgetWrapper.append($.parseHTML((0, _Widget2.default)()));
                _this.initializeApp();
            });
        }
    }, {
        key: "initializeApp",
        value: function initializeApp() {
            this.queryElements();
            this.initializeCitiesDropdown();
            this.initializeHotelList();
        }
    }, {
        key: "queryElements",
        value: function queryElements() {
            this.$cityDropdown = this.$widgetWrapper.find(selectors.CITY_DROPDOWN_WRAPPER);
            this.$citySearchInput = this.$cityDropdown.find('input');

            this.$cityDropdownList = this.$cityDropdown.find(selectors.CITY_DROPDOWN_LIST);
            this.$cityDropdownListItems = this.$cityDropdown.find('li');

            this.$hotelList = $(selectors.HOTEL_LIST);
        }
    }, {
        key: "initializeCitiesDropdown",
        value: function initializeCitiesDropdown() {
            var _this2 = this;

            this.$citySearchInput.on('input', function (event) {
                _this2.$cityDropdownListItems.each(function (_, listItem) {
                    var $listItem = $(listItem);
                    var itemText = $listItem.text().toLowerCase();

                    if (itemText.startsWith(_this2.$citySearchInput.val().toLowerCase())) {
                        $listItem.show();
                    } else {
                        $listItem.hide();
                    }
                });
            });

            this.$citySearchInput.click(function () {
                _this2.$cityDropdown.toggleClass('active');
                _this2.$cityDropdownList.slideToggle(300);
            });

            this.$cityDropdown.focusout(function () {
                _this2.$cityDropdown.removeClass('active');
                _this2.$cityDropdownList.slideUp(300);
            });
        }
    }, {
        key: "_loadHotels",
        value: function _loadHotels(cityName) {
            var _this3 = this;

            var pageNumber = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;
            var appendHotels = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

            _index2.default.get($, cityName, pageNumber).done(function (data) {
                var hotels = data['results'].map(_Hotel2.default);

                if (!appendHotels) {
                    _this3.$hotelList.empty();
                }
                _this3.$hotelList.unbind('scroll');
                _this3.$hotelList.append(hotels);

                if (data['next']) {
                    _this3.$hotelList.on('scroll', function () {
                        var scrollPercentage = (_this3.$hotelList[0].scrollTop + _this3.$hotelList[0].clientHeight) / _this3.$hotelList[0].scrollHeight;

                        if (scrollPercentage > 0.9) {
                            _this3.$hotelList.unbind('scroll');
                            _this3._loadHotels(cityName, pageNumber + 1, true);
                        }
                    });
                }
            });
        }
    }, {
        key: "initializeHotelList",
        value: function initializeHotelList() {
            var _this4 = this;

            this.$cityDropdownListItems.click(function (event) {
                var cityName = $(event.target).text();
                _this4.$citySearchInput.val(cityName);
                _this4._loadHotels(cityName);
            });
        }
    }]);

    return App;
}();

exports.default = App;

},{"./api/config":4,"./api/hotels/index":6,"./api/utils":7,"./components/Hotel":9,"./components/Widget":10,"./constants/selectors":12}],9:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function (_ref) {
  var name = _ref.name,
      image_url = _ref.image_url,
      city = _ref.city,
      price = _ref.price;

  return "\n        <li class=\"hotel-item js-hotel-list-item\">\n          <img src=\"" + image_url + "\" alt=\"" + name + "\">\n          <div class=\"hotel-description\">\n            <span class=\"hotel-description-name\">" + name + "</span>\n            <span class=\"hotel-description-city\">" + city + "</span>\n            <span class=\"hotel-description-price\">" + price + "</span>\n          </div>\n        </li>\n    ";
};

},{}],10:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.default = function () {
    return "\n    <div id=\"hotel-widget-container\">\n        <div>\n        <span class=\"city-choose-text\">\u0412\u044B\u0431\u0435\u0440\u0438\u0442\u0435 \u0433\u043E\u0440\u043E\u0434</span>\n        <div class=\"city-dropdown-wrapper js-city-dropdown-wrapper\">\n            <input class=\"city-search-input\" type=\"text\" name=\"city\" placeholder=\"\u0412\u044B\u0431\u0435\u0440\u0438\u0442\u0435 \u0433\u043E\u0440\u043E\u0434\">\n            <ul class=\"city-dropdown-list js-city-dropdown-list\">\n            <li>\u0420\u043E\u0441\u0442\u043E\u0432-\u043D\u0430-\u0414\u043E\u043D\u0443</li>\n            <li>\u041C\u044E\u043D\u0445\u0435\u043D</li>\n            <li>\u041A\u0430\u0437\u0430\u043D\u044C</li>\n            <li>\u0421\u0430\u043C\u0430\u0440\u0430</li>\n            <li>\u0428\u0430\u0440\u0434\u0436\u0430</li>\n            <li>\u041B\u043E\u043E</li>\n            <li>\u042F\u043B\u0442\u0430</li>\n            <li>\u041D\u044F\u0447\u0430\u043D\u0433</li>\n            <li>\u041A\u0440\u0430\u0441\u043D\u0430\u044F \u041F\u043E\u043B\u044F\u043D\u0430</li>\n            <li>\u041C\u0438\u043B\u0430\u043D</li>\n            <li>\u0412\u0438\u0442\u044F\u0437\u0435\u0432\u043E</li>\n            <li>\u0413\u043E\u043D\u043A\u043E\u043D\u0433</li>\n            <li>\u0415\u0432\u043F\u0430\u0442\u043E\u0440\u0438\u044F</li>\n            <li>\u0425\u0435\u043B\u044C\u0441\u0438\u043D\u043A\u0438</li>\n            <li>\u0411\u0443\u0434\u0430\u043F\u0435\u0448\u0442</li>\n            <li>\u041B\u0430\u0437\u0430\u0440\u0435\u0432\u0441\u043A\u043E\u0435</li>\n            <li>\u041F\u0441\u043A\u043E\u0432</li>\n            <li>\u041D\u043E\u0432\u043E\u0441\u0438\u0431\u0438\u0440\u0441\u043A</li>\n            <li>\u0412\u0435\u043D\u0435\u0446\u0438\u044F</li>\n            <li>\u041D\u044C\u044E-\u0419\u043E\u0440\u043A</li>\n            <li>\u041E\u0434\u0435\u0441\u0441\u0430</li>\n            <li>\u0421\u0430\u043D\u043A\u0442-\u041F\u0435\u0442\u0435\u0440\u0431\u0443\u0440\u0433</li>\n            <li>\u041F\u0430\u0442\u0442\u0430\u0439\u044F</li>\n            <li>\u0411\u0435\u0440\u043B\u0438\u043D</li>\n            <li>\u041A\u0438\u0441\u043B\u043E\u0432\u043E\u0434\u0441\u043A</li>\n            <li>\u0414\u043D\u0435\u043F\u0440\u043E\u043F\u0435\u0442\u0440\u043E\u0432\u0441\u043A</li>\n            <li>\u0425\u0443\u0440\u0433\u0430\u0434\u0430</li>\n            <li>\u0425\u0430\u0431\u0430\u0440\u043E\u0432\u0441\u043A</li>\n            <li>\u0428\u0430\u0440\u043C-\u044D\u043B\u044C-\u0428\u0435\u0439\u0445</li>\n            <li>\u0421\u0438\u043D\u0433\u0430\u043F\u0443\u0440</li>\n            <li>\u0411\u043E\u043B\u044C\u0448\u043E\u0439 \u0413\u0435\u043B\u0435\u043D\u0434\u0436\u0438\u043A</li>\n            <li>\u0421\u0442\u0430\u043C\u0431\u0443\u043B</li>\n            <li>\u0412\u0430\u0440\u0448\u0430\u0432\u0430</li>\n            <li>\u041B\u0430\u0441-\u0412\u0435\u0433\u0430\u0441</li>\n            <li>\u041C\u043E\u0441\u043A\u0432\u0430</li>\n            <li>\u0421\u0443\u0437\u0434\u0430\u043B\u044C</li>\n            <li>\u041F\u0435\u0442\u0440\u043E\u0437\u0430\u0432\u043E\u0434\u0441\u043A</li>\n            <li>\u0421\u0430\u043C\u0443\u0438</li>\n            <li>\u0427\u0435\u043B\u044F\u0431\u0438\u043D\u0441\u043A</li>\n            <li>\u0410\u043C\u0441\u0442\u0435\u0440\u0434\u0430\u043C</li>\n            <li>\u041B\u044C\u0432\u043E\u0432</li>\n            <li>\u041D\u0438\u0436\u043D\u0438\u0439 \u041D\u043E\u0432\u0433\u043E\u0440\u043E\u0434</li>\n            <li>\u041C\u0438\u043D\u0441\u043A</li>\n            <li>\u042D\u0439\u043B\u0430\u0442</li>\n            <li>\u041F\u044F\u0442\u0438\u0433\u043E\u0440\u0441\u043A</li>\n            <li>\u0410\u043D\u0430\u043F\u0430</li>\n            <li>\u041A\u0438\u0435\u0432</li>\n            <li>\u0413\u0435\u043B\u0435\u043D\u0434\u0436\u0438\u043A</li>\n            <li>\u041F\u0430\u0440\u0438\u0436</li>\n            <li>\u0422\u044E\u043C\u0435\u043D\u044C</li>\n            <li>\u042F\u0440\u043E\u0441\u043B\u0430\u0432\u043B\u044C</li>\n            <li>\u041B\u043E\u043D\u0434\u043E\u043D</li>\n            <li>\u041F\u0435\u0440\u043C\u044C</li>\n            <li>\u0422\u0435\u043B\u044C-\u0410\u0432\u0438\u0432</li>\n            <li>\u0421\u0443\u0434\u0430\u043A</li>\n            <li>\u0412\u043B\u0430\u0434\u0438\u043C\u0438\u0440</li>\n            <li>\u0420\u0438\u043C</li>\n            <li>\u0428\u0430\u0440\u043A\u0441 \u0411\u0435\u0439</li>\n            <li>\u0414\u0436\u0435\u043C\u0435\u0442\u0435</li>\n            <li>\u0412\u0435\u043D\u0430</li>\n            <li>\u041A\u0430\u0431\u0430\u0440\u0434\u0438\u043D\u043A\u0430</li>\n            <li>\u041A\u0440\u0430\u0441\u043D\u043E\u044F\u0440\u0441\u043A</li>\n            <li>\u0412\u0435\u043B\u0438\u043A\u0438\u0439 \u041D\u043E\u0432\u0433\u043E\u0440\u043E\u0434</li>\n            <li>\u042D\u0441\u0442\u043E-\u0421\u0430\u0434\u043E\u043A</li>\n            <li>\u0421\u043E\u0447\u0438</li>\n            <li>\u041A\u0435\u043C\u0435\u0440</li>\n            <li>\u0415\u043A\u0430\u0442\u0435\u0440\u0438\u043D\u0431\u0443\u0440\u0433</li>\n            <li>\u0411\u0430\u043D\u0433\u043A\u043E\u043A</li>\n            <li>\u0412\u0435\u043B\u0438\u043A\u0438\u0439 \u0423\u0441\u0442\u044E\u0433</li>\n            <li>\u0418\u0440\u043A\u0443\u0442\u0441\u043A</li>\n            <li>\u041A\u043E\u043A\u0442\u0435\u0431\u0435\u043B\u044C</li>\n            <li>\u0422\u0430\u043B\u043B\u0438\u043D</li>\n            <li>\u041F\u0445\u0443\u043A\u0435\u0442</li>\n            <li>\u041C\u0430\u0434\u0440\u0438\u0434</li>\n            <li>\u0420\u043E\u0434\u043E\u0441</li>\n            <li>\u0411\u0430\u0440\u0441\u0435\u043B\u043E\u043D\u0430</li>\n            <li>\u0425\u0430\u0440\u044C\u043A\u043E\u0432</li>\n            <li>\u0413\u043E\u0430</li>\n            <li>\u0421\u0435\u0443\u043B</li>\n            <li>\u041A\u0430\u043B\u0438\u043D\u0438\u043D\u0433\u0440\u0430\u0434</li>\n            <li>\u041A\u0440\u0430\u0441\u043D\u043E\u0434\u0430\u0440</li>\n            <li>\u041A\u0430\u043D\u043A\u0443\u043D</li>\n            <li>\u0422\u0431\u0438\u043B\u0438\u0441\u0438</li>\n            <li>\u0410\u0434\u043B\u0435\u0440</li>\n            <li>\u0420\u0438\u0433\u0430</li>\n            <li>\u0414\u0443\u0431\u0430\u0439</li>\n            <li>\u0422\u0443\u043B\u0430</li>\n            <li>\u041E\u043C\u0441\u043A</li>\n            <li>\u0414\u043E\u043C\u0431\u0430\u0439</li>\n            <li>\u0421\u0435\u0432\u0430\u0441\u0442\u043E\u043F\u043E\u043B\u044C</li>\n            <li>\u0423\u0444\u0430</li>\n            <li>\u0421\u0438\u043C\u0444\u0435\u0440\u043E\u043F\u043E\u043B\u044C</li>\n            <li>\u0412\u0438\u043B\u044C\u043D\u044E\u0441</li>\n            <li>\u0421\u0432\u0435\u0442\u043B\u043E\u0433\u043E\u0440\u0441\u043A</li>\n            <li>\u0424\u0435\u043E\u0434\u043E\u0441\u0438\u044F</li>\n            <li>\u0412\u043E\u0440\u043E\u043D\u0435\u0436</li>\n            <li>\u0412\u043E\u043B\u0433\u043E\u0433\u0440\u0430\u0434</li>\n            <li>\u0410\u043B\u0443\u043F\u043A\u0430</li>\n            <li>\u0410\u043B\u0443\u0448\u0442\u0430</li>\n            <li>\u041F\u0440\u0430\u0433\u0430</li>\n            <li>\u041A\u043E\u0441\u0442\u0440\u043E\u043C</li>\n          </ul>\n        </div>\n        </div>\n        \n        <ul class=\"hotel-list js-hotel-list\">\n        </ul>\n    </div>\n    ";
};

},{}],11:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var JQUERY_VERSION_TO_LOAD = exports.JQUERY_VERSION_TO_LOAD = '3.3.1';

},{}],12:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var WIDGET_CONTAINER_WRAPPER_SELECTOR = exports.WIDGET_CONTAINER_WRAPPER_SELECTOR = '#hotel-widget-container-wrapper';
var CITY_DROPDOWN_WRAPPER = exports.CITY_DROPDOWN_WRAPPER = '.js-city-dropdown-wrapper';
var CITY_DROPDOWN_LIST = exports.CITY_DROPDOWN_LIST = '.js-city-dropdown-list';
var HOTEL_LIST = exports.HOTEL_LIST = '.js-hotel-list';

},{}],13:[function(require,module,exports){
"use strict";

var _app = require("./app");

var _app2 = _interopRequireDefault(_app);

var _jquery = require("./jquery");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(0, _jquery.getjQuery)(function ($) {
    new _app2.default($);
});

},{"./app":8,"./jquery":14}],14:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.getjQuery = undefined;

var _common = require('./constants/common');

var loadjQuery = function loadjQuery(verison, onLoadHandler) {
    var scriptTag = document.createElement('script');
    scriptTag.setAttribute('type', 'text/javascript');
    scriptTag.setAttribute('src', 'http://ajax.googleapis.com/ajax/libs/jquery/' + verison + '/jquery.min.js');

    if (scriptTag.readyState) {
        // For old versions of IE
        scriptTag.onreadystatechange = function () {
            if (this.readyState === 'complete' || this.readyState === 'loaded') {
                onLoadHandler();
            }
        };
    } else {
        // Other browsers
        scriptTag.onload = onLoadHandler;
    }

    (document.getElementsByTagName('head')[0] || document.documentElement).appendChild(scriptTag);
};

var getjQuery = exports.getjQuery = function getjQuery(onRetrieveHandler) {
    // ToDo: проверить, возможно, в некоторых версиях не работает
    if (window.jQuery === undefined) {
        loadjQuery(_common.JQUERY_VERSION_TO_LOAD, function () {
            var jQuery = window.jQuery.noConflict(true);
            onRetrieveHandler(jQuery);
        });
    } else {
        onRetrieveHandler(window.jQuery);
    }
};

},{"./constants/common":11}]},{},[13]);
